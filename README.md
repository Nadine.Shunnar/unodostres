# UnoDosTres

## Installation Instructions
### Python Version
This project has been made for Python 3.8 and above. Older versions of Python may work, but they have not been thoroughly tested.

### Depedencies
Install Pillow using the following commands:

For Mac, Windows, or Linux
```
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade Pillow
```
## Running UNO
To run UNO run the uno_game.py script. If Pillow is installed correctly you will be prompted with a menu to start the game.


