import tkinter as tk
from tkinter import *
from tkinter.ttk import *
from PIL import ImageTk, Image
import random
from datetime import datetime
import sys
from time import sleep

# window width and height
ww = 1400
wh = 900


class Uno_Game:

    def __init__(self):
        self.initial_frame = tk.Tk()
        self.initial_frame.title('Uno')
        # Set up initial frame with width and height of set size
        win_h = self.initial_frame.winfo_screenheight()
        win_w = self.initial_frame.winfo_screenwidth()
        x = int((win_w - ww) / 2)
        y = int((win_h - wh) / 2)
        self.initial_frame.geometry(f"{ww}x{wh}+{int(x)}+{int(y)}")
        # Start game boolean
        self.quit = True

        self.user_deck_images = []
        self.user_deck_buttons = []
        self.active_deck_image = ImageTk.PhotoImage(Image.open("cards/back.png").convert('RGBA'))
        self.state = tk.ACTIVE
        self.user_buttons = tk.Button
        self.user_card = tk.StringVar()

        self.button_style = Style()
        self.button_style.configure('TButton', font=('calibri', 10, 'bold', 'underline'), foreground='black')
        self.button_style.map('TButton', foreground=[('active', 'red')], background=[('active', 'black')])
        self.popup = None
        # Constant UNO_DECK
        self.UNO_DECK = [(-1, 'b$'), (-1, 'b+'), (0, 'b0'), (1, 'b1'), (2, 'b2'), (3, 'b3'), (4, 'b4'), (5, 'b5'),
                         (6, 'b6'), (7, 'b7'),
                         (8, 'b8'), (9, 'b9'), (-1, 'b@'), (-1, 'g$'), (-1, 'g+'), (0, 'g0'), (1, 'g1'), (2, 'g2'),
                         (3, 'g3'), (4, 'g4'), (5, 'g5'), (6, 'g6'),
                         (7, 'g7'), (8, 'g8'), (9, 'g9'), (-1, 'g@'), (-1, 'kw+'), (-1, 'kw'), (-1, 'r$'), (-1, 'r+'),
                         (0, 'r0'), (1, 'r1'), (2, 'r2'),
                         (3, 'r3'), (4, 'r4'), (5, 'r5'), (6, 'r6'), (7, 'r7'), (8, 'r8'), (9, 'r9'), (-1, 'r@'),
                         (-1, 'y$'), (-1, 'y+'), (0, 'y0'), (1, 'y1'), (2, 'y2'),
                         (3, 'y3'), (4, 'y4'), (5, 'y5'), (6, 'y6'), (7, 'y7'), (8, 'y8'), (9, 'y9'), (-1, 'y@')]

        self.Active_Deck = []  # Deck that Player draws card from
        self.Discard_Deck = []  # Deck that Player puts down card into
        # Deck: User/ Computer (1,2,3)
        self.user_cards = []
        self.computer_1_cards = []
        self.computer_2_cards = []
        self.computer_3_cards = []

        self.top_card = ()  # Card that is at top of the Discard Deck--> Player must match conditions of this card
        self.card_match = False
        self.is_special = False
        self.reverse = False
        self.direction_arrow = 1  # Even: going in forward direction     Odd: going in reverse direction
        self.color_match = ''


        random.seed(datetime.now())
        self.player = random.randint(0, 3)  # To know who plays first --> initialized when Uno_Game is called\


        self.shuffle()
        self.discard_image = ImageTk.PhotoImage(Image.open("cards/front_" + self.Discard_Deck[-1][1] + ".png").convert('RGBA'))

        # fileName, height, width
        im_root1 = self.get_img('Uno_logo.png', 400, 400)
        img_computer1 = Label(self.initial_frame, image=im_root1)
        img_computer1.place(x=515, y=50)

        self.start_game_window()

        self.initial_frame.mainloop()

    """
        Inital window showing 2 buttons- start or quit
        Start button--> def start_uno()
        Quit button --> def end_game()
    """
    def start_game_window(self):
        # Create buttons in the Initial (start, quit)


        start_button = Button(self.initial_frame, text='Start Game',
                              command=lambda: [self.initial_frame, self.start_uno()])

        quit_button = Button(self.initial_frame, text="Quit Game",
                             command=lambda: self.end_game())

        start_button.place(x=630, y=500, width=150, height=50)
        quit_button.place(x=630, y=550, width=150, height=50)

    """
        Start_Uno displays the TopLevel window where the game will sit
            Displays each players cards and loops through each players turns
            Functionality of the game is here: increasing player turns and calling proper functions on selected cards
    """
    def start_uno(self):

        # create the next window
        self.initial_frame.withdraw()
        uno_window = tk.Toplevel(self.initial_frame)
        win_h = uno_window.winfo_screenheight()
        win_w = uno_window.winfo_screenwidth()
        x = int((win_w - ww) / 2)
        y = int((win_h - wh) / 2)
        uno_window.configure(background='#006400')
        uno_window.geometry(f"{ww}x{wh}+{int(x)}+{int(y)}")
        quit_button = Button(uno_window, text="Quit Game",
                             command=lambda: self.end_game())
        quit_button.place(x=0, y=0, width=150, height=50)

        active_deck_button = tk.Button
        dis_button = tk.Button

        active_deck_button(uno_window, command=lambda: {self.draw_card(0), self.update_user_deck_gui(uno_window)},
                           image=self.active_deck_image, borderwidth=0, highlightthickness = 0).place(x=500, y=200)



        im_root1 = self.get_img('Bob.png', 100, 100)
        img_computer1 = Label(uno_window, text='welcome', image=im_root1)
        img_computer1.place(x=70, y=200, width=100, height=100)

        im_root2 = self.get_img('Eddy.png', 100, 100)
        img_computer2 = Label(uno_window, text='welcome', image=im_root2)
        img_computer2.place(x=600, y=10, width=100, height=100)

        im_root3 = self.get_img('Jason.png', 100, 100)
        img_computer3 = Label(uno_window, text='welcome', image=im_root3)
        img_computer3.place(x=1230, y=200, width=100, height=100)


        # Loops through whole game until a player has either: quit or a won(0 cards left in deck)
        while len(self.user_cards) != 0 and len(self.computer_1_cards) != 0 and len(self.computer_2_cards) != 0 and \
                len(self.computer_3_cards) != 0 and self.quit:

            for img in range(len(self.user_cards)):
                self.user_deck_images.append((self.user_cards[img][1], ImageTk.PhotoImage(
                    Image.open("cards/front_" + self.user_cards[img][1] + ".png").convert('RGBA'))))

            #GUI UPDATES in next two lines
            self.update_discard_deck_gui(dis_button, uno_window)

            self.update_user_deck_gui(uno_window)

            # Do everything separate- .place and .grid return None of NoneType== errors

            text1 = tk.Text(uno_window, width=15, height=1)
            text1.place(x=50, y=300)
            self.show_computer(1, text1)

            text2 = tk.Text(uno_window, width=10, height=1)
            text2.place(x=605, y=110)
            self.show_computer(2, text2)

            text3 = tk.Text(uno_window, width=10, height=1)
            text3.place(x=1245, y=300)
            self.show_computer(3, text3)


            if self.player == 0:
                self.card_match = False
                self.is_special = False
                found_card = None
                # if not valid
                # wait for another user input
                while (self.card_match == False or found_card is None) and self.quit != False:
                    # wait for user
                    self.user_deck_buttons[0].wait_variable(self.user_card)
                    card_chosen = self.user_card.get()  # Type String
                    card = self.convert_str_tup_user(card_chosen)  # Returns tuple

                    # Check if it is valid

                    found_card = self.find_match(card)  # Returns the card --> sets card_match = True and is_special == false.
                    # If special returns card/ sets is_special == True
                    if self.is_special and (found_card is not None):
                        can_be_played = self.validate_special_card(found_card)
                        if can_be_played:
                            self.handle_special_card(found_card)
                            self.card_match = True
                        else:
                            self.card_match = False

                if self.is_special != True:  # self.player already handled if special card was played in handle_special_card
                    if self.reverse == False:
                        self.player = self.player + 1
                        if self.player > 3:
                            self.player = 0
                    else:
                        self.player = self.player - 1
                        if self.player < 0:
                            self.player = 3
                if(self.quit != False):
                    self.play_the_card(0, found_card)
                    self.initial_frame.after(1500, self.f(dis_button, uno_window, self.user_buttons))

            if len(self.user_cards) == 0:  # if when it comes to their turn the player has 0 cards left--> break from the loop == Winner
                break
            # update the discard deck gui

            if self.player == 1 and self.quit != False:
                self.computer_turn(1)

                self.show_computer(1, text1)

                self.initial_frame.after(1500, self.f(dis_button, uno_window, self.user_buttons))
                if(self.quit != False):
                    self.initial_frame.after(1500,self.f(dis_button,uno_window,self.user_buttons))
                if len(self.computer_1_cards) == 0:
                    break
                if self.is_special != True:  # self.player already handled if special card was played in handle_special_card
                    if self.reverse == False:
                        self.player = self.player + 1
                        if self.player > 3:
                            self.player = 0
                    else:
                        self.player = self.player - 1
                        if self.player < 0:
                            self.player = 3
                # self.print_stuff()

            if self.player == 2 and self.quit != False:
                self.computer_turn(2)

                self.show_computer(2, text2)

                self.initial_frame.after(1500, self.f(dis_button, uno_window, self.user_buttons))
                if len(self.computer_2_cards) == 0:
                    break

                if self.is_special != True:  # self.player already handled if special card was played in handle_special_card
                    if self.reverse == False:
                        self.player = self.player + 1
                        if self.player > 3:
                            self.player = 0
                    else:
                        self.player = self.player - 1
                        if self.player < 0:
                            self.player = 3
                # self.print_stuff()
            if self.player == 3 and self.quit != False:
                self.computer_turn(3)
                self.initial_frame.after(1500, self.f(dis_button, uno_window, self.user_buttons))

                self.show_computer(3, text3)

                if len(self.computer_3_cards) == 0:
                    break
                if self.is_special != True:  # self.player already handled if special card was played in handle_special_card
                    if self.reverse == False:
                        self.player = self.player + 1
                        if self.player > 3:
                            self.player = 0
                    else:
                        self.player = self.player - 1
                        if self.player < 0:
                            self.player = 3
        #         self.print_stuff()
        # self.print_stuff()
        if len(self.user_cards) == 0:
            self.winning_popup(0)
            # self.end_game()
        elif len(self.computer_1_cards) == 0:
            self.winning_popup(1)
            # self.end_game()
        elif len(self.computer_2_cards) == 0:
            self.winning_popup(2)
            # self.end_game()
        elif len(self.computer_3_cards) == 0:
            self.winning_popup(3)
            # self.end_game()

    #Display the who the winner is
    def winning_popup(self, winner):
        win = tk.Toplevel()
        win.geometry("256x256")
        message = ''
        if winner == 0:
            win.title('You Win!')
            message = 'You won! Congratulations!'
        else:
            win.title('Game Over!')
            message = 'Player ' + str(winner) + ' won! Congratulations!'
        Label(win, text=message).pack()
        Button(win, text='End Game', command=lambda: [win.destroy(), self.end_game()]).pack()
        win.wait_window()
    """
        Backend Functionality of updating the game GUI
    """
    def f(self, discard_button, uno_window, user_buttons):
        self.update_discard_deck_gui(discard_button, uno_window)
        self.update_user_deck_gui(uno_window)

        self.initial_frame.update()
        sleep(1)

    """
        Functionality of computer turns 1/2/3 
        Checks for validity of card and if special card (using functions)
        plays card if validate, draws card if no valid card in deck
    """
    def computer_turn(self, computer_number):
        i = 0
        found_card = None
        self.card_match = False
        self.is_special = False

        if computer_number == 1:
            while not self.card_match and i < len(self.computer_1_cards):  # loop through the entire deck of computer card until a card match is found
                found_card = self.find_match(self.computer_1_cards[i])
                if self.is_special and (found_card is not None):
                    can_be_played = self.validate_special_card(found_card)
                    if can_be_played:
                        self.handle_special_card(found_card)
                        self.card_match = True
                    else:
                        self.card_match = False
                i = i + 1
            if found_card is None:
                while self.card_match != True:
                    draw_card = self.draw_card(computer_number)
                    found_card = self.find_match(draw_card)
                    if self.is_special and found_card is not None:
                        can_be_played = self.validate_special_card(found_card)
                        if can_be_played:
                            self.handle_special_card(found_card)
                            self.card_match = True
                        else:
                            self.card_match = False

            elif found_card is not None and self.card_match == False: #found card is not none and card_match is false
                while self.card_match != True:
                    draw_card = self.draw_card(computer_number)
                    found_card = self.find_match(draw_card)
                    if self.is_special and found_card is not None:
                        can_be_played = self.validate_special_card(found_card)
                        if can_be_played:
                            self.handle_special_card(found_card)
                            self.card_match = True
                        else:
                            self.card_match = False
            # Falls out of while loop because a card match was found --> play the card
            self.play_the_card(computer_number, found_card)
            # now is the next player
        if computer_number == 2:
            while not self.card_match and i < len(
                    self.computer_2_cards):  # loop through the entire deck of computer card until a card match is found
                found_card = self.find_match(self.computer_2_cards[i])
                if self.is_special and (found_card is not None):
                    can_be_played = self.validate_special_card(found_card)
                    if can_be_played:
                        self.handle_special_card(found_card)
                        self.card_match = True
                    else:
                        self.card_match = False
                i = i + 1
            if found_card is None:
                while self.card_match != True:
                    draw_card = self.draw_card(computer_number)
                    found_card = self.find_match(draw_card)
                    if self.is_special and found_card is not None:
                        can_be_played = self.validate_special_card(found_card)
                        if can_be_played:
                            self.handle_special_card(found_card)
                            self.card_match = True
                        else:
                            self.card_match = False

            elif found_card is not None and self.card_match == False: #found card is not none and card_match is false
                while self.card_match != True:
                    draw_card = self.draw_card(computer_number)
                    found_card = self.find_match(draw_card)
                    if self.is_special and found_card is not None:
                        can_be_played = self.validate_special_card(found_card)
                        if can_be_played:
                            self.handle_special_card(found_card)
                            self.card_match = True
                        else:
                            self.card_match = False
            # Falls out of while loop because a card match was found --> play the card
            self.play_the_card(computer_number, found_card)
        if computer_number == 3:
            while not self.card_match and i < len(
                    self.computer_3_cards):  # loop through the entire deck of computer card until a card match is found
                found_card = self.find_match(self.computer_3_cards[i])
                if self.is_special and (found_card is not None):
                    can_be_played = self.validate_special_card(found_card)
                    if can_be_played:
                        self.handle_special_card(found_card)
                        self.card_match = True
                    else:
                        self.card_match = False
                i = i + 1
            if found_card is None:
                while self.card_match != True:
                    draw_card = self.draw_card(computer_number)
                    found_card = self.find_match(draw_card)
                    if self.is_special and found_card is not None:
                        can_be_played = self.validate_special_card(found_card)
                        if can_be_played:
                            self.handle_special_card(found_card)

                            self.card_match = True
                        else:
                            self.card_match = False

            elif found_card is not None and self.card_match == False: #found card is not none and card_match is false
                while self.card_match != True:
                    draw_card = self.draw_card(computer_number)
                    found_card = self.find_match(draw_card)
                    if self.is_special and found_card is not None:
                        can_be_played = self.validate_special_card(found_card)
                        if can_be_played:
                            self.handle_special_card(found_card)
                            self.card_match = True
                        else:
                            self.card_match = False
            # Falls out of while loop because a card match was found --> play the card
            self.play_the_card(computer_number, found_card)

    """
        Can the selected card be played against the discard deck?
        Checks to see if number matches, color matches or if special card
        if no match, then return None
        
    """
    def find_match(self, card):
        # if the number of the Discard Deck and the number of the computer_card match --> play the card
        if self.Discard_Deck[-1][0] == card[0] and (self.Discard_Deck[-1][0] != -1):
            self.color_match = card[1][0]
            self.card_match = True
            self.is_special = False
            return card
        # If the color of the Discard Deck and the color of the computer_card match --> play the card
        elif ((self.Discard_Deck[-1][1][0] == card[1][0]) or (self.color_match == card[1][0])) and (self.is_special_card(card) != True):
            self.color_match = card[1][0]
            self.card_match = True
            self.is_special = False
            return card
        elif self.is_special_card(card):
            return card
        else:
            return None

    """
       Checks and Handles reverse, skip, plus2, plus4 
    """

    def handle_special_card(self, card):
        if '@' in card[1]:  # skip
            if self.reverse:  # If reverse is true
                self.player = self.player - 2
                if self.player == -1:
                    self.player = 3
                elif self.player == -2:
                    self.player = 2
            else:  # if reverse is False
                self.player = self.player + 2
                if self.player == 4:
                    self.player = 0
                elif self.player == 5:
                    self.player = 1
            self.color_match = card[1][0]
        elif '$' in card[1]:  # reverse
            if self.reverse == False:
                self.direction_arrow = 1
                self.reverse = True
                self.player = self.player -1
                if self.player < 0:
                    self.player = 3
            else:
                self.direction_arrow = 2
                self.reverse = False
                self.player = self.player + 1
                if self.player > 3:
                    self.player = 0
            self.color_match = card[1][0]

# DO: add the cards to the next player not the current one
        elif '+' in card[1] and 'w' not in card[1]:  # +2 card
            if not self.reverse:
                if self.player == 0:
                    for i in range(0, 2):
                        if len(self.Active_Deck) != 0:
                            self.computer_1_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_1_cards.append(self.Active_Deck.pop(0))


                elif self.player == 1:
                    for i in range(0, 2):
                        if len(self.Active_Deck) != 0:
                            self.computer_2_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_2_cards.append(self.Active_Deck.pop(0))
                elif self.player == 2:
                    for i in range(0, 2):
                        if len(self.Active_Deck) != 0:
                            self.computer_3_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_3_cards.append(self.Active_Deck.pop(0))
                elif self.player == 3:
                    for i in range(0, 2):
                        if len(self.Active_Deck) != 0:
                            self.user_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.user_cards.append(self.Active_Deck.pop(0))
                self.color_match = card[1][0]

                self.player = self.player + 1
                if self.player > 3:
                    self.player = 0

            elif self.reverse:
                if self.player == 0:
                    for i in range(0, 2):
                        if len(self.Active_Deck) != 0:
                            self.computer_3_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_3_cards.append(self.Active_Deck.pop(0))

                elif self.player == 1:
                    for i in range(0, 2):
                        if len(self.Active_Deck) != 0:
                            self.user_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.user_cards.append(self.Active_Deck.pop(0))
                elif self.player == 2:
                    for i in range(0, 2):
                        if len(self.Active_Deck) != 0:
                            self.computer_1_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_1_cards.append(self.Active_Deck.pop(0))
                elif self.player == 3:
                    for i in range(0, 2):
                        if len(self.Active_Deck) != 0:
                            self.computer_2_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_2_cards.append(self.Active_Deck.pop(0))
                self.color_match = card[1][0]
                self.player = self.player -1
                if self.player <0:
                    self.player = 3
        elif 'w+' in card[1]:  # +4 card
            if not self.reverse:
                if self.player == 0:
                    self.choice_window()
                    for i in range(0, 4):
                        if len(self.Active_Deck) != 0:
                            self.computer_1_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_1_cards.append(self.Active_Deck.pop(0))

                elif self.player == 1:
                    self.choose_random_color()
                    for i in range(0, 4):
                        if len(self.Active_Deck) != 0:
                            self.computer_2_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_2_cards.append(self.Active_Deck.pop(0))
                elif self.player == 2:
                    self.choose_random_color()
                    for i in range(0, 4):
                        if len(self.Active_Deck) != 0:
                            self.computer_3_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_3_cards.append(self.Active_Deck.pop(0))
                elif self.player == 3:
                    self.choose_random_color()
                    for i in range(0, 4):
                        if len(self.Active_Deck) != 0:
                            self.user_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.user_cards.append(self.Active_Deck.pop(0))
                self.player = self.player +1
                if self.player > 3:
                    self.player = 0
            elif self.reverse:
                if self.player == 0:
                    self.choice_window()
                    for i in range(0, 4):
                        if len(self.Active_Deck) != 0:
                            self.computer_3_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_3_cards.append(self.Active_Deck.pop(0))

                elif self.player == 1:
                    self.choose_random_color()
                    for i in range(0, 4):
                        if len(self.Active_Deck) != 0:
                            self.user_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.user_cards.append(self.Active_Deck.pop(0))
                elif self.player == 2:
                    self.choose_random_color()
                    for i in range(0, 4):
                        if len(self.Active_Deck) != 0:
                            self.computer_1_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_1_cards.append(self.Active_Deck.pop(0))
                elif self.player == 3:
                    self.choose_random_color()
                    for i in range(0, 4):
                        if len(self.Active_Deck) != 0:
                            self.computer_2_cards.append(self.Active_Deck.pop(0))
                        else:
                            self.shuffle()
                            self.computer_2_cards.append(self.Active_Deck.pop(0))
                self.player = self.player -1
                if self.player < 0:
                    self.player = 3
        elif 'w' in card[1] and '+' not in card[1]:  # wild card choose color
            if self.player == 0:
                self.choice_window()
                if not self.reverse:
                    self.player = self.player + 1
                    if self.player > 3:
                        self.player = 0
                elif self.reverse:
                    self.player = self.player - 1
                    if self.player < 0:
                        self.player = 3
            else:
                # computer picks the color to match
                self.choose_random_color()
                if not self.reverse:
                    self.player = self.player +1
                    if self.player > 3:
                        self.player = 0
                elif self.reverse:
                    self.player = self.player -1
                    if self.player < 0:
                        self.player = 3

    def validate_special_card(self, card):
        if ('w' in card[1]):
            return True
        elif ((card[1][1] == '@' and self.Discard_Deck[-1][1][1] == '@') or (card[1][1] == '$' and self.Discard_Deck[-1][1][1] == '$') or (card[1][1] == '+' and self.Discard_Deck[-1][1][1] == '+')):
            return True
        elif(card[1][0] == self.color_match):
            return True
        else:
            return False
    """
    If computer turn wants to play wild card--> random choosing of color to be played
    """
    def choose_random_color(self):
        random_letter = random.randint(0, 3)
        if random_letter == 0:
            self.color_match = 'b'
        elif random_letter == 1:
            self.color_match = 'r'
        elif random_letter == 2:
            self.color_match = 'y'
        else:
            self.color_match = 'g'

    """ 
        Turns tuple in to string for backend functionality
    """
    def convert_str_tup_user(self,string_card):
        to_int = 0
        to_string = ""
        disallowed_characters = "()''"
        for character in disallowed_characters:
            string_card = string_card.replace(character, "")
        to_int, to_string = string_card.split(", ",1)
        to_int  = int(to_int)
        to_tuple = (to_int, to_string)

        return to_tuple

    def print_stuff(self):
        print()
        print()
        print("User Deck: ", self.user_cards)
        print("Comp 1 Deck: ", self.computer_1_cards)
        print("Comp 2 Deck : ", self.computer_2_cards)
        print("Comp 3 Deck: ", self.computer_3_cards)
        print("Discard Deck: ", self.Discard_Deck)
        print("Active Deck: ", self.Active_Deck)
        print("Color match: ", self.color_match)
        print("Player: ", self.player)
        print("Card match(T/F): ", self.card_match)
        print("Is special(T/F): ", self.is_special)
        print("Is reverse (T/F", self.reverse)
        print()
        print()

    def is_special_card(self, card):
        if ('+' in card[1][1] or '@' in card[1][1] or '$' in card[1][1] or ('w' in card[1][1])):
            self.is_special = True
            return True
        else:
            return False

    """ 
        If the card is valid- play the card
        append to respective deck
        delete from respecitve deck
    """
    def play_the_card(self, computer_number, card_to_play):
        if(computer_number == 0):
            self.user_cards.remove(card_to_play)
            self.Discard_Deck.append(card_to_play)
            # self.print_stuff()
        if (computer_number == 1):
            # remove card from user deck
            self.computer_1_cards.remove(card_to_play)
            # put card in back of discard deck
            self.Discard_Deck.append(card_to_play)

        if (computer_number == 2):
            # remove card from user deck
            self.computer_2_cards.remove(card_to_play)
            # put card in back of discard deck
            self.Discard_Deck.append(card_to_play)
        if (computer_number == 3):
            # remove card from user deck
            self.computer_3_cards.remove(card_to_play)
            # put card in back of discard deck
            self.Discard_Deck.append(card_to_play)


    """
        Draw card in to respective deck if user hits the draw card button or computer has no valid cards that
        can be played
    """
    def draw_card(self, computer_number):
        if (len(self.Active_Deck) == 0):
            self.shuffle()
            draw_card = self.Active_Deck.pop(0)
            # add draw_card to computer deck
            if (computer_number == 1):
                self.computer_1_cards.append(draw_card)
            if (computer_number == 2):
                self.computer_2_cards.append(draw_card)
            if (computer_number == 3):
                self.computer_3_cards.append(draw_card)
            if (computer_number == 0):
                self.user_cards.append(draw_card)


        else:
            draw_card = self.Active_Deck.pop(0)
            if (computer_number == 1):
                self.computer_1_cards.append(draw_card)
            if (computer_number == 2):
                self.computer_2_cards.append(draw_card)
            if (computer_number == 3):
                self.computer_3_cards.append(draw_card)
            if(computer_number == 0):
                self.user_cards.append(draw_card)
        return draw_card

    """
        Backend Shuffle the cards randomly and assign 7 cards to each player
        
    """
    def shuffle(self):
        # shuffle all of the self.UNO_DECK CARDS
        if len(self.Discard_Deck) == 0:

            # Shuffle the UNO cards and populate the Active Deck
            random.shuffle(self.UNO_DECK)
            self.Active_Deck = self.UNO_DECK.copy()

            # Populate Player cards
            for i in range(7):
                self.user_cards.append(self.Active_Deck.pop(0))  # Pop from the top of the deck
                self.computer_1_cards.append(self.Active_Deck.pop(0))
                self.computer_2_cards.append(self.Active_Deck.pop(0))
                self.computer_3_cards.append(self.Active_Deck.pop(0))

            # Populate the Discard Deck with the top card of the Active Deck
            # Find the first non-functional card
            for i in range(len(self.Active_Deck)-1):
                if ("w" not in self.Active_Deck[len(self.Active_Deck)-i-1][1] and "@" not in self.Active_Deck[len(self.Active_Deck)-i-1][1]
                and "+" not in self.Active_Deck[len(self.Active_Deck)-i-1][1] and "$" not in self.Active_Deck[len(self.Active_Deck)-i-1][1]
                and "%" not in self.Active_Deck[len(self.Active_Deck)-i-1][1]):
                    self.Discard_Deck.append(self.Active_Deck.pop(len(self.Active_Deck)-i-1))
                    break
                else:
                    pass

            self.color_match = self.Discard_Deck[-1][1][0]
        elif ((len(self.user_cards) + len(self.computer_1_cards) + len(self.computer_2_cards) + len(self.computer_3_cards)) >= (len(self.UNO_DECK) - 1)):
            self.end_game()
        else:
            self.top_card = self.Discard_Deck.pop(-1)  # Take from the end of the Discard LIST (represents top of the discard deck that user is looking at)
            for card in self.Discard_Deck:
                if 'w' in card[1]:
                    if len(card[1]) == 2:
                        new_card = list(card)
                        new_card[1] = 'kw'
                        card = tuple(new_card)
                    else:
                        new_card = list(card)
                        new_card[1] = 'kw+'
                        card = tuple(new_card)
            random.shuffle(self.Discard_Deck)
            self.Active_Deck = self.Discard_Deck.copy()
            self.Discard_Deck = [self.top_card]
            self.color_match = self.top_card[1][0]


    """
        If player quits: end game
        if player wins: end game
    """

    def end_game(self):
        self.quit = False
        m = (7,'b7')
        self.user_card.set(m)
        self.initial_frame.destroy()

        sys.exit()

    # -------GUI FUNCTIONS BELOW----------
    def choice_window(self):
        color_choice = tk.Toplevel()
        color_choice.title('Wild Card!')
        message = 'Choose a new color!'
        Label(color_choice, text=message).pack()
        Button(color_choice, text='Red', command=lambda: [self.make_choice('r'), color_choice.destroy()]).pack()
        Button(color_choice, text='Green', command=lambda: [self.make_choice('g'), color_choice.destroy()]).pack()
        Button(color_choice, text='Blue', command=lambda: [self.make_choice('b'), color_choice.destroy()]).pack()
        Button(color_choice, text='Yellow', command=lambda: [self.make_choice('y'), color_choice.destroy()]).pack()
        color_choice.wait_window()

    def make_choice(self, color):
        self.color_match = color

    def waiting_window(self):
        self.popup = None
        self.popup = tk.Toplevel()
        self.popup.geometry("0x0")
        self.popup.title('')
        self.popup.withdraw()
        self.popup.wait_window()

    """
        Updates the discard deck after each tur
    """
    def update_discard_deck_gui(self,discard_button,uno_window_yes):

        if "w" in self.Discard_Deck[-1][1]:
            if "w+" in self.Discard_Deck[-1][1]:
                self.Discard_Deck[-1] = (self.Discard_Deck[-1][0], self.color_match + "w+")
            else:
                self.Discard_Deck[-1] = (self.Discard_Deck[-1][0], self.color_match + "w")

        self.discard_image = ImageTk.PhotoImage(Image.open("cards/front_" + self.Discard_Deck[-1][1] + ".png").convert('RGBA'))
        discard_button(uno_window_yes, image=self.discard_image, borderwidth=0, highlightthickness = 0).place(x=900, y=200)


        text1 = tk.Text(uno_window_yes, width=15, height=1)
        text1.place(x=300, y=300)
        self.show_turn(self.player, text1)
    """
        Updates the users deck after each turn
        if computer plays a +4 on the user: will add 4 cards to user deck
    """
    def update_user_deck_gui(self, uno_window):
        for deck_button in self.user_deck_buttons:
            deck_button.destroy()

        self.user_deck_buttons = []

        self.user_deck_images = []

        for img in range(len(self.user_cards)):
            if "w" in self.user_cards[img][1]:
                if "w+" in self.user_cards[img][1]:
                    self.user_deck_images.append((self.user_cards[img][1], ImageTk.PhotoImage(Image.open("cards/front_kw+.png").convert('RGBA'))))
                else:
                    self.user_deck_images.append((self.user_cards[img][1], ImageTk.PhotoImage(Image.open("cards/front_kw.png").convert('RGBA'))))
            else:
                self.user_deck_images.append((self.user_cards[img][1], ImageTk.PhotoImage(Image.open("cards/front_" + self.user_cards[img][1] + ".png").convert('RGBA'))))

        if len(self.user_cards) <= 7:
            for card in range(len(self.user_cards)):
                user_button = self.user_buttons(uno_window,
                                                command=lambda m=self.user_cards[card]: self.user_card.set(m),
                                                image=self.user_deck_images[card][1], state=self.state, borderwidth=0, highlightthickness = 0)
                self.user_deck_buttons.append(user_button)
                self.user_deck_buttons[card].place(x=250 + card * 120, y=450)

        # Adding second row of cards
        elif len(self.user_cards) > 7:
            for card in range(7):
                user_button = self.user_buttons(uno_window,
                                                command=lambda m=self.user_cards[card]: self.user_card.set(m),
                                                image=self.user_deck_images[card][1], state=self.state, borderwidth=0, highlightthickness = 0)
                self.user_deck_buttons.append(user_button)
                self.user_deck_buttons[card].place(x=250 + card * 120, y=450)
            if len(self.user_cards) <= 14:
                for card in range(7, len(self.user_cards)):
                    user_button = self.user_buttons(uno_window,
                                                    command=lambda m=self.user_cards[card]: self.user_card.set(m),
                                                    image=self.user_deck_images[card][1], state=self.state, borderwidth=0, highlightthickness = 0)
                    self.user_deck_buttons.append(user_button)
                    self.user_deck_buttons[card].place(x=250 + (card - 7) * 120, y=550)

            # Adding third row of cards if needed
            if len(self.user_cards) > 14:
                for card in range(7, len(self.user_cards)):
                    user_button = self.user_buttons(uno_window,
                                                    command=lambda m=self.user_cards[card]: self.user_card.set(m),
                                                    image=self.user_deck_images[card][1], state=self.state, borderwidth=0, highlightthickness = 0)
                    self.user_deck_buttons.append(user_button)
                    self.user_deck_buttons[card].place(x=250 + (card - 7) * 120, y=550)
                for card in range(14, len(self.user_cards)):
                    user_button = self.user_buttons(uno_window,
                                                    command=lambda m=self.user_cards[card]: self.user_card.set(m),
                                                    image=self.user_deck_images[card][1], state=self.state, borderwidth=0, highlightthickness = 0)
                    self.user_deck_buttons.append(user_button)
                    self.user_deck_buttons[card].place(x=250 + (card - 14) * 120, y=650)


    """
        Whos turn is it? Displays this
    """
    def show_turn(self, player, text: tk.Text):
        text.configure(fg = "white", font =("Quicksand",20),background = "dark green", borderwidth=0, highlightthickness = 0,width = 15)
        if player == 0:
            text.delete(0.0, tk.END)
            text.insert(tk.INSERT, "Your Turn")
            text.update()

        else:
            names = ["Be Cool Bob", "Jimothy", "Json"]
            text.delete(0.0, tk.END)
            text.insert(tk.INSERT, f"{names[player - 1]}'s Turn")
            text.update()

    """
        Displays the name of the computer players
        Be cool bob
        jimothy
        json
    """
    def show_computer(self, player, text: tk.Text):
        text.configure(fg = "white", font =("Quicksand",20),background = "dark green", borderwidth=0, highlightthickness = 0)
        if player == 1:
            text.delete(0.0, tk.END)
            text.insert(tk.INSERT, f"Be Cool Bob: {len(self.computer_1_cards)}")
            text.update()

        elif player == 2:
            text.delete(0.0, tk.END)
            text.insert(tk.INSERT, f"Jimothy: {len(self.computer_2_cards)}")
            text.update()

        elif player == 3:
            text.delete(0.0, tk.END)
            text.insert(tk.INSERT, f"Json: {len(self.computer_3_cards)}")
            text.update()

    def get_img(self, fileName, width, height):
        im = Image.open(fileName).resize((width, height))
        im = ImageTk.PhotoImage(im)
        return im


app = Uno_Game()
